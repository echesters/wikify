'''
Created on 1 Feb 2014

@author: Elizabeth
'''
from math import log
from articles import find_articles_linking_to_article
from Wikify_Project.models import Articles, Article_Links

def calculate_log_max(main_linking_articles, second_linking_articles):
    num_main_articles = len(main_linking_articles)
    num_second_articles = len(second_linking_articles)
    return log(max(num_main_articles, num_second_articles))

def calculate_relatedness(articles_a, articles_b, all_articles):
    log_max = calculate_log_max(articles_a, articles_b)
    intersected_articles = intersection(articles_a, articles_b)
    if intersected_articles:
        log_both_articles = log(len(intersected_articles))
        top_equation = log_max - log_both_articles
        
        log_all_articles = log(len(all_articles))
        log_min = log(min(len(articles_a), len(articles_b)))
        bottom_equation = log_all_articles - log_min
        
        relatedness = top_equation / bottom_equation
        return relatedness
    return 0

def determine_relatedness(links_a, article_b, article_count):
    article_b_links = Article_Links.get_links_in_article(article_b)
    return calculate_relatedness(links_a, article_b_links, article_count)
    

def intersection(articles_A, articles_B):
    return [val for val in articles_A if val in articles_B]
