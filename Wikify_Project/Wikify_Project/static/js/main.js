$(document).ready(function() {
    $('.title').hide().fadeIn("slow");
    $('.sub-title').hide().fadeIn("slow");

  var onClick = function(e) {
    e.preventDefault();
    $(this).tab('show');
  };

  $('#format-wiki a').click(onClick);
  $('#format-html a').click(onClick);
  $('#format-preview a').click(onClick);

  var renderhtml = $("#html").find("p").text();
  $('#rendered_content').html(renderhtml);
});