$(document).ready(function() {


    $( ".tabbed-area" ).tabs({ active: 0 });
    var active = $( ".tabbed-area" ).tabs( "option", "active" );
	$( ".tabbed-area" ).tabs( "option", "active", 0 );



    $(".fancybox").fancybox({
	    openEffect  : 'none',
	    closeEffect : 'none'
	});

	$('#contact_form').submit(function(e) {
		e.preventDefault();
		var inputs = {};

		$('input:not(#send), textarea', '#contact_form').each(function() {
			inputs[$(this).attr('name')] = $(this).val();
		});

		var csrftoken = getCookie('csrftoken');

		$.ajax({
			beforeSend: function(xhr, settings) {
		        if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
		            // Send the token to same-origin, relative URLs only.
		            // Send the token only if the method warrants CSRF protection
		            // Using the CSRFToken value acquired earlier
		            xhr.setRequestHeader("X-CSRFToken", csrftoken);
		        }
		    },
			type:'POST',
			url: '/contact/',
			data: inputs,
			success: function(data) {
				$('#contact_form').replaceWith(data);
			},
			error: function(errorThrown) {
				console.log(errorThrown.responseText);
			}
		});
	});
});

 $(function() {
    $( ".tabs" ).tabs();
  });

 // using jQuery
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
function sameOrigin(url) {
    // test that a given url is a same-origin URL
    // url could be relative or scheme relative or absolute
    var host = document.location.host; // host + port
    var protocol = document.location.protocol;
    var sr_origin = '//' + host;
    var origin = protocol + sr_origin;
    // Allow absolute or scheme relative URLs to same origin
    return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
        (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
        // or any other URL that isn't scheme relative or absolute i.e relative.
        !(/^(\/\/|http:|https:).*/.test(url));
}