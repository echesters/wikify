'''
Created on 7 Feb 2014

@author: Elizabeth
'''
import os
import re

def is_text_file(file_name):
    return file_name.endswith(".txt")

def get_articles(files_dir):
    all_articles = {}  
    article_file_names = filter(is_text_file, os.listdir(files_dir))
    
    os.chdir('files')
    for file_name in article_file_names:
        with open(file_name, 'r') as textfile:
            title = re.sub('.txt', '', file_name)
            all_articles[title] = textfile.read()
    return all_articles

'''
    all_matches is [link, term]
    all_words is every word that is used a link
        used to count how many times a word is a link
'''
def find_every_link(all_articles):
    all_matches = []
    all_link_words = []
    
    pattern = re.compile('[\[[]+[\w.]+[\s]?[(\w\s.)]+[|]+[\w\s.]+]]')
    single_pattern = re.compile('[\[[]+[\w\s.]+]]')
    
    for _, article_text in all_articles.items():
        matches = re.findall(pattern, article_text)
        single_matches = re.findall(single_pattern, article_text)
    
        for m in matches:
            new_m = re.sub('[\[\]]', '', m)
            words = new_m.split("|")
            all_matches.append([words[0], words[1]])
            all_link_words.append(words[1])
        for sm in single_matches:
            new_sm = re.sub('[\[\]]', '', sm)
            all_matches.append([new_sm, new_sm])
            all_link_words.append(new_sm)
    return all_link_words, all_matches

'''
    find all links in given article
'''
def find_all_links_in_article(article):
    all_matches = []
    
    pattern = re.compile('[\[[]+[\w]+[\s]?[(\w\s)]+[|]+[\w]+]]')
    single_pattern = re.compile('[\[[]+[\w\s]+]]')
    
    matches = re.findall(pattern, article)
    single_matches = re.findall(single_pattern, article)

    for m in matches:
        new_m = re.sub('[\[\]]', '', m)
        words = new_m.split("|")
        all_matches.append([words[0], words[1]])
    for sm in single_matches:
        new_sm = re.sub('[\[\]]', '', sm)
        all_matches.append([new_sm, new_sm])
        
    return all_matches

'''
returns array of every sense the word has been associated with
''' 
def find_associated_articles(word, all_matches):
    matched_terms = set()
    for m in all_matches:
        if string_found(word, m[1]): #[link/sense | term/word]
            if m[0] not in matched_terms:
                matched_terms.add(m[0])
    return matched_terms
  
def string_found(string1, string2):
    return re.search(r"\b" + re.escape(string1) + r"\b", string2)  

# return all articles which link to article_name
# return all articles which link to the link we're finding in article_name
def find_articles_linking_to_article(article_name, linked_article_name, articles):
    found_articles = set()
    found_linked_secondary_articles = set()
    for article in articles:
        if article_name in article:
            found_articles.add(article)
        if linked_article_name in article:
            found_linked_secondary_articles.add(article)
    return found_articles, found_linked_secondary_articles