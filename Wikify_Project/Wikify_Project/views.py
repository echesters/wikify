'''
Created on 30 Sep 2013

@author: Elizabeth
'''
from django.shortcuts import render, render_to_response
from handle_get import respond_html, respond_wikify
from Wikify_Project.models import WikiForm
from django.core.mail import send_mail, BadHeaderError
from django.http.response import HttpResponse
from django.conf import settings
from django.core.mail.message import EmailMessage

def index(request):
    if 'original' in request.GET:
        original_text = request.GET.get('original', '')
        wikified_text = respond_wikify(original_text)
        html_text = respond_html(original_text)
        
        wikiform = WikiForm(initial={ 'original':original_text,
                                       'wikified': wikified_text,
                                       'HTML': html_text
                                    })
    else:
        wikiform = WikiForm()
        html_text = ''
    return render_to_response('index.html', {'wikiform':wikiform,
                                             'htmllabel': html_text })
    
def intro(request):
    return render_to_response('intro.html')