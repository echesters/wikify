'''
Created on 1 Apr 2014

@author: EChesters
'''
from Wikify_Project.models import Commonness_Values
import operator
from Wikify_Project.relatedness import determine_relatedness

def get_sense(important_words):
    '''
        From an article's relatedness and commonness, find each word's
        sense/article to link to.
    '''
    word_and_sense = {}
    
    for word in important_words:
        commonness = get_commonness(word)
        relatedness = get_relatedness(word, important_words, commonness.keys())
        word_and_sense.update({word: get_best_article(commonness.get(word), relatedness.get(word))})
        
    return word_and_sense
        
def get_commonness(word):
    ''' 
        Find commonness score of one word. 
            returns {word: score}
    '''
    commonness = {}
    
    comm_articles = Commonness_Values.get_commonness_articles_matching_word(word)
    if comm_articles:
        commonness.update(comm_articles)
    
    return commonness

def get_relatedness(word, words, articles):
    ''' 
        Find relatedness score of one word, using overlap of all important words. 
            returns {word:score}
    '''
    relatedness = {}
    score = determine_relatedness(words, word, len(articles))
    
    relatedness.update({word: score})
    return relatedness

def get_best_article(commonness, relatedness):
    ''' 
        Gets score for each article and averages between the scores, 
            return the highest score 
    '''
    articles = commonness.keys()
    
    overall_scores = { article: calculate_average(commonness.get(article), relatedness) for article in articles }
    sorted_scores = sorted(overall_scores.iteritems(), key=operator.itemgetter(1), reverse=True)
    return sorted_scores[0][0]
    
def calculate_average(commonness_value, relatedness_value):
    ''' Calculate average between relatedness and commonness score '''
    return (commonness_value + relatedness_value)/2
