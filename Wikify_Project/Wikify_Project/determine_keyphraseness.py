'''
Created on 2 Feb 2014

@author: Elizabeth
'''
from Wikify_Project.models import Keyphraseness

def find_words_with_keyphraseness(raw_text):
    keyphraseness_values = Keyphraseness.get_keyphraseness_values()
    possible_important_words = find_single_words(raw_text, keyphraseness_values)
    possible_important_words.update(find_ngrams(raw_text, keyphraseness_values))
    
    if possible_important_words:        
        sorted_by_score = sorted(possible_important_words, key = lambda x: x[1], reverse=True)
        percent = percentage_of_links(raw_text)
        return sorted_by_score[:percent]
    return []
    
def find_single_words(raw_text, keyphraseness_values):
    single_words = {}
    for word in raw_text.words: 
        if word in keyphraseness_values or word.lower() in keyphraseness_values or word.capitalize() in keyphraseness_values:
            single_words.update({word: keyphraseness_values.get(word)})
    return single_words

def find_ngrams(raw_text, keyphraseness_values):
    ngram_words = {}
    for x in range(2, 5):
        n_grams = raw_text.ngrams(n=x)
        for ngram in n_grams:
            words = ' '.join(ngram)
            if words in keyphraseness_values:
                ngram_words.update({words: keyphraseness_values.get(words)})
    return ngram_words
    
def percentage_of_links(words):
    percentage = int((len(words.split()) * 0.1))
    if(percentage < 1):
        return 1
    return percentage