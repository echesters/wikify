'''
Created on 6 Nov 2013

@author: Elizabeth
'''
from textblob import TextBlob as tb
from determine_keyphraseness import find_words_with_keyphraseness

from determine_sense import get_sense

def respond_wikify(original):
    concat = lambda article, term: "[[" + article + " | " + term + "]]"
    
    important_words = determine_important_words(tb(original))
    wikified = original;
    
    for key_word in important_words:
        link_article = important_words.get(key_word)
        wikified = wikified.replace(key_word, concat(link_article, key_word), 1)
    
    return wikified

def determine_important_words(raw_text):
    keyphrase_words = find_words_with_keyphraseness(raw_text)
    return get_sense(keyphrase_words)

def respond_html(original):
    concat = lambda article, term: '<a href="http://en.wikipedia.org/wiki/' + article + '">' + term + '</a>'
    important_words = determine_important_words(tb(original))
    
    html = original;
    for key_word in important_words:
        link_article = important_words.get(key_word)
        html = html.replace(key_word, concat(link_article, key_word), 1)
    return html