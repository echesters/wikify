'''
Created on 18 Feb 2014

@author: Elizabeth
'''
from django.test import TestCase
from Wikify_Parsing.models import Commonness_Values

class Commonness_Test(TestCase):
    def test_commonness_returns_dictionary(self):
        important_word = 'tree'
        actual_articles = Commonness_Values.get_commonness_articles_matching_word(important_word)
        print actual_articles
        expected_articles = {u'tree': [[ u'tree (graph theory)', 100]]}
        self.assertEqual(expected_articles, actual_articles, "Commonness is returning wrong structure")
        self.assertTrue(type(actual_articles) is dict, "Commonness articles should be a dictionary")