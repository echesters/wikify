'''
Created on 21 Nov 2013

@author: Elizabeth
'''
from django.db import models
from django import forms

from itertools import chain

class WikiForm(forms.Form):
    original = forms.CharField(widget=forms.Textarea(attrs={'class':'form-control'}))
    wikified = forms.CharField(widget=forms.Textarea(attrs={'class':'form-control'}))
    HTML = forms.CharField(widget=forms.Textarea(attrs={'class':'form-control'}))
    
class Keyphraseness(models.Model):
    word = models.CharField(max_length=100)
    keyphraseness = models.FloatField()
    
    class Meta:
        db_table = 'term_keyphraseness'
        
    @staticmethod
    def get_keyphraseness_values():
        keyphrase_dict = {}
        for row in Keyphraseness.objects.values():
            keyphrase_dict.update({row['word']: row['keyphraseness']})
        return keyphrase_dict

class Articles(models.Model):
    article = models.CharField(max_length = 100)
    
    class Meta:
        db_table = "articles"
        
    @staticmethod
    def get_articles_containing_link(word):
        return Articles.objects.filter(article_links__link=word)
    
class Article_Links(models.Model):
    link = models.CharField(max_length=100)
    articles = models.ManyToManyField(Articles)
    
    class Meta:
        db_table = "article_links"
        
    @staticmethod
    def get_links_in_article(article):
        return Article_Links.objects.filter(articles__article = article)

    
class Commonness_Values(models.Model):
    term = models.CharField(max_length=100)
    commonness = models.FloatField()
    article = models.ForeignKey(Articles)
    
    class Meta:
        db_table = "commonness_values"
    
    @staticmethod
    def get_commonness_articles_matching_word(word):
        comm_articles = {}
        try:
            commonness_articles = Commonness_Values.objects.filter(term=word).order_by('-commonness')
            comm_element = {}
            for comm_article in commonness_articles:
                comm_element.update({comm_article.article.article: comm_article.commonness})
                comm_articles.update({ word: comm_element })
            
            return comm_articles
        except Exception, err:
            print err
        return []

class Relatedness_Values(models.Model):
    term = models.CharField(max_length=100)
    relatedness = models.FloatField()
    article = models.ForeignKey(Articles)
     
    class Meta:
        db_table = "relatedness_values"
         
    @staticmethod
    def get_relatedness_articles_matching_word(word):
        rel_articles = {}
        try:
            relatedness_articles = Relatedness_Values.objects.filter(term=word).order_by('-relatedness')
#             articles =  
            rel_articles_two = Relatedness_Values.objects.filter(term=word.capitalize()).order_by('-relatedness')
            relatedness_articles = chain(rel_articles_two, relatedness_articles)
            rel_element = {}
            for rel_article in relatedness_articles:
                rel_element.update({rel_article.article.article: [rel_article.relatedness, rel_article.id]})
                rel_articles.update({ word: rel_element })
            return rel_articles
        except Exception, err:
            print err
        return {}
